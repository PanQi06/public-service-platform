import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
//定义状态
const state = {
	token: null,
    pageStack: [],
    userInfo: {}
};
//定义改变状态的方法
const mutations = {
	setUserInfo(state, userInfo) {
		return localStorage.setItem('salesUserInfo', userInfo);
	},
	getUserInfo(state) {
		state.userInfo = localStorage.getItem('salesUserInfo');
	},
	setToken(state, TOKEN) {
		return localStorage.setItem('salesToken', TOKEN);
	},
	getToken(state) {
		state.token = localStorage.getItem('salesToken');
	}
}
export default new Vuex.Store({
    state,mutations
});
