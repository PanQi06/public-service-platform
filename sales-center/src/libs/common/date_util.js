let formatTimestamp = function(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate() + ' ';
    h = date.getHours() + ':';
    m = date.getMinutes() + ':';
    s = date.getSeconds();
    return Y+M+D+h+m+s;
}

let formatTime = function(time) {
	 if (time <= 60) {
        time = time + '秒';
        return time;
    } else if (time > 60 && time < 60 * 60) {
        time = parseInt(time / 60) + "分钟";
        return time;
    } else {
        var hour = parseInt(time / 3600) + "小时";
        var minute = parseInt(parseInt(time % 3600) / 60) + "分钟";
        time = hour + minute;
        return time;
    }
}

export {formatTimestamp, formatTime}
