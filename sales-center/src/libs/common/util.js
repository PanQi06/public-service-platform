import axios from 'axios';

let util = {

};
util.title = function (title) {
    title = title ? title + '|管理端' : '管理端';
    window.document.title = title;
};

console.log(process.env.NODE_ENV);

const ajaxUrl = process.env.NODE_ENV === 'development' ?
    'http://fwpt.zpark-imway.com:8099/psp-sellcenter' :
    process.env.NODE_ENV === 'production' ?
        'http://fwpt.zpark-imway.com:8099/psp-sellcenter' :
        'http://fwpt.zpark-imway.com:8099/psp-sellcenter';
util.baseUrl = ajaxUrl;

util.ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000
});

export default util;