import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/seller/v1/login',
    'resetPwd': '/seller/v1/resetPwd',//更改密码
    'updateName': '/seller/v1/updateName',//更改用户名

    'getLoginImgCode': '/file/v1/getLoginImgCode',//获取图片验证码
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
            imgCode: params.imgCode
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getLoginImgCode (userId,w,h) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getLoginImgCode, {
            userId: userId,
            w: w,
            h: h
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function resetPwd (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetPwd, {
            oldPwd: params.oldPwd,
            password: params.password,
            confirmPwd: params.confirmPwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

export default {
    login,getLoginImgCode,updateName,resetPwd
}