import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    //工单相关接口
    'getOrders': '/order/v1/getOrders', //获取工单信息列表
    'getOrderNum': '/order/v1/getOrderNum', //获取工单数量
    'addWorkOrder': '/order/v1/add', //新增客户工单
    'getServiceProviders': '/order/v1/getServiceProviders', //获取服务分类及服务商列表
    'getDetail': '/order/v1/getDetail', //获取工单详情
    'getOrderLogs': '/order/v1/getOrderLogs', //获取工单操作日志

    'allotOrder': '/order/v1/allotOrder', //分配工单给服务商
    'closeOrder': '/order/v1/closeOrder', //关闭工单
    'uploadContract': '/order/v1/uploadContract', //上传合同
    'confirmOrder': '/order/v1/confirmOrder', //确认工单
    'feedback': '/order/v1/feedback', //工单反馈
};

let res = {

};
function getOrders (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrders, {
            uid: uid,
            page: params.currentPage,
            pagesize: params.pagesize,
            stage: params.stage,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderNum(stage) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderNum, {
            stage: stage
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function addWorkOrder (uid, pid, label, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addWorkOrder, {
            uid: uid,
            pid: pid,
            label: label,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getServiceProviders() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getServiceProviders, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs(oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function allotOrder(oid, pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.allotOrder, {
            oid: oid,
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function closeOrder(oid, status, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.closeOrder, {
            oid: oid,
            status: status,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function uploadContract(oid, params, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.uploadContract, {
            oid: oid,
            contractNo: params.contractNo,
            name: params.name,
            signTime: params.signTime,
            startTime: params.startTime,
            endTime: params.endTime,
            partyA: params.partyA,
            partyB: params.partyB,
            contractUrl: params.contractUrl,
            payment: params.payment,
            paymentWay: params.paymentWay,
            payDesc: params.payDesc,
            service: params.service,
            money: params.money,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function confirmOrder(oid, type, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.confirmOrder, {
            oid: oid,
            type: type,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function feedback(oid, score, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.feedback, {
            oid: oid,
            score: score,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getOrders: getOrders,
    getOrderNum: getOrderNum,
    addWorkOrder: addWorkOrder,
    getServiceProviders: getServiceProviders,
    getDetail: getDetail,
    getOrderLogs: getOrderLogs,
    allotOrder: allotOrder,
    closeOrder: closeOrder,
    uploadContract: uploadContract,
    confirmOrder: confirmOrder,
    feedback: feedback
}