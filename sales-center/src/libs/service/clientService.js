import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getUsers': '/user/v1/getUsers', // 客户列表
    'getUserNum': '/user/v1/getUserNum', //获取客户总数
    'add': '/user/v1/add', //新增客户
    'edit': '/user/v1/edit', //编辑客户
    'editLabel': '/user/v1/editLabel', //编辑标签
    'editLevel': '/user/v1/editLevel', //编辑客户级别
    'archive': '/user/v1/archive', //归档客户
    'getDetail': '/user/v1/getDetail', //获取客户详情
    'getUserLogs': '/user/v1/getUserLogs',//获取客户操作日志
    //客户消息相关接口
    'getUserNews': '/usernews/v1/getUserNews',//获取客户消息流
    'addClientNews': '/usernews/v1/add',//新增客户消息
    //文件详情接口
    'getQINIUToken': '/file/v1/getQINIUToken',//获取qiniutoken
    'uploadImage': '/file/v1/uploadImage', //上传文件
};

let res = {

};

function getUsers (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType,
            status: params.status
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserNum (status) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNum, {
            status: status
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function addClient (params,label, isUpdate, isClaim) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.add, {
            ctype: params.ctype,
            name: params.name,
            phoneNum: params.phoneNum,
            companyName: params.companyName,
            position: params.position,
            visitDest: params.visitDest, //参观目的
            visitNum: params.visitNum, //参观人数
            refCompany: params.refCompany, //推荐单位
            referrer: params.referrer, //推荐人
            visitTime: params.visitTime, //参观时间 参数格式2018-04-10 03:00
            escort: params.escort, //陪同人
            introducer: params.introducer, //引导介绍人
            visitflow: params.visitflow, //参观流程
            remark: params.remark, //备注
            label: label,
            isUpdate: isUpdate,
            isClaim: isClaim
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function editClient (uid, params, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.edit, {
            userId: uid,
            name: params.name,
            phoneNum: params.phoneNum,
            companyName: params.companyName,
            position: params.position,
            label: label
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function editLabel (userId, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLabel, {
            userId: userId,
            label: label
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function editLevel (userId, level) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLevel, {
            userId: userId,
            level: level
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function archive (userId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.archive, {
            userId: userId
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            uid: uid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserLogs (uid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserLogs, {
            uid: uid,
            key: key
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserNews (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            uid: uid,
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addClientNews(uid, label, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addClientNews, {
            uid: uid,
            label: label,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getQINIUToken() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getQINIUToken, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function uploadImage() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.uploadImage, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getUsers: getUsers,
    getUserNum: getUserNum,
    addClient: addClient,
    editClient: editClient,
    editLabel: editLabel,
    editLevel: editLevel,
    archive: archive,
    getDetail: getDetail,
    getUserLogs: getUserLogs,
    getUserNews: getUserNews,
    addClientNews: addClientNews,
    getQINIUToken: getQINIUToken,
    uploadImage: uploadImage
}