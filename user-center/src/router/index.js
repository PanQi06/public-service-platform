import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'login',
      component: () =>
        import ('@/pages/login/loginPage'),
      redirect: '/login',
      children: [{
        path: '/login',
        name: 'loginMainPanel',
        component: () =>
          import ('@/pages/login/view/loginMainPanel')
      }]
    },
    {
      path: '/index',
      name: 'index',
      redirect: '/personalInfo',
      component: () =>
        import ('@/pages/indexPage'),
        children: [
            {
                path: '/personalInfo',
                name: 'personalInfo',
                component: () => 
                    import('@/pages/personalInfo/personalInfoPage')
            },
            {
                path: '/companyInfo',
                name: 'companyInfo',
                component: () => 
                    import('@/pages/companyInfo/companyInfoPage')
            },
            {
                path: '/order',
                name: 'order',
                component: () => 
                    import('@/pages/order/orderPage')
            },
            {
                path: '/orderDetail',
                name: 'orderDetail',
                component: () => 
                    import('@/pages/order/orderDetail')
            },
            {
                path: '/userInfo',
                name: 'userInfo',
                component: () => 
                    import('@/pages/userInfo/userInfoPage')
            }
        ]
    },
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ('@/pages/register/registerPage'),
        redirect: '/register',
        children: [{
            path: '/register',
            component: () =>
            import ('@/pages/register/view/register')
        }]
    },
    { path: '*', redirect: '/login', }
  ]
})
