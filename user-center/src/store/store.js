import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
//定义状态
const state = {
  token: null,
  currentPath: [{
    title: '首页',
    path: '/index',
    name: 'index'
  }],
  account: null,
  loginParam: null
};
//定义改变状态的方法
const mutations = {
  setAccount(state, account) {
    if (account != null) {
      localStorage.setItem('account', JSON.stringify(account));
      localStorage.setItem('account-token', account.token);
      state.account = account;
      state.token = account.token;
    }
  },
  setLoginParam(state, param) {
    if (param != null) {
      localStorage.setItem('login-param-phoneNum', param.phoneNum);
      localStorage.setItem('login-param-pwd', param.password);
      localStorage.setItem('login-param-rem', param.rem);
    }
  },

  clearLoginParam(state, phoneNum) {
    localStorage.removeItem('login-param-pwd');
    localStorage.setItem('login-param-phoneNum', phoneNum);
    localStorage.setItem('login-param-rem', false);
  },

  loginOut(state) {
    localStorage.removeItem('account');
    localStorage.removeItem('account-token');
    localStorage.removeItem('current-path');
    state.account = null;
    state.token = null;
  },

  setCurrentPath(state, pathArr) {
    state.currentPath = pathArr;
    if (pathArr != null) {
      localStorage.setItem('current-path', JSON.stringify(pathArr));
    }
  },
}
const getters = {
  loginParam: (state) => {
    state.loginParam = {
      phoneNum: localStorage.getItem('login-param-phoneNum'),
      password: localStorage.getItem('login-param-pwd'),
      rem: localStorage.getItem('login-param-rem')
    }
    // console.log("loginParam", state.loginParam);
    return state.loginParam;
  },
  token: (state) => {
    if (state.token == null || state.token == 'undefined') {
      // console.log("state.token", state.token)
      var accStr = localStorage.getItem('account');
      if (accStr != null) {
        var account = JSON.parse(accStr);
        state.account = account;
        state.token = localStorage.getItem('account-token');
        // console.log(state.token);
      }
    }
    return state.token;
  },
  account: (state) => {
    if (state.account == null) {
      var accStr = localStorage.getItem('account');
      if (accStr != null) {
        var account = JSON.parse(accStr);
        state.account = account;
        state.token = localStorage.getItem('account-token');
      }
    }
    return state.account;
  },
  getCurrentPath: (state) => {
    var currentPath = localStorage.getItem('current-path');
    if (currentPath != null) {
      var current = JSON.parse(currentPath);
      state.currentPath = current;
    }
    return state.currentPath;
  }
}
export default new Vuex.Store({
  state,
  mutations,
  getters
});
