import axios from 'axios';

let util = {

};
util.title = function (title) {
    title = title ? title + '|管理端' : '管理端';
    window.document.title = title;
};

console.log(process.env.NODE_ENV);

const ajaxUrl = process.env.NODE_ENV === 'development' ?
    'http://47.93.144.246/psp2-web' :
    process.env.NODE_ENV === 'production' ?
        'http://47.93.144.246/psp2-web' :
        'http://47.93.144.246/psp2-web';
util.baseUrl = ajaxUrl;

util.ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000
});

export default util;