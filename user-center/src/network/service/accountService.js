import util from '@/network/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/user/v1/login',
    'getUser': '/user/v1/getUser',// 获取个人信息
    'updateUser': '/user/v1/updateUser',// 修改个人信息
    'getUserOrderList': '/user/v1/getUserOrderList',// 获取个人工单列表
    'getUserOrder': '/user/v1/getUserOrder',// 查看工单详情
    'getUserEnterprise': '/user/v1/getUserEnterprise',// 根据用户id查询用户企业信息
    'getEnterpriseName': '/user/v1/getEnterpriseName',// 根据用户name查询用户企业信息
    'addOrUpdateEnterprise': '/user/v1/addOrUpdateEnterprise',// 新增、修改企业信息
    'getIndustryBeanList': '/user/v1/getIndustryBeanList',// 获取行业信息列表
    'getOrderLogs': '/user/v1/getOrderLogs',// 获取操作日志
    'getUserNews': '/user/v1/getUserNews',// 获取客户信息流
    'addUser': '/user/v1/addUser',// 注册
    'updateUserPassword': '/user/v1/updateUserPassword',// 修改密码

    'getWechatQRCode': '/order/v1/getWechatQRCode', // 获取微信支付二维码
    'payBack': '/order/v1/payBack', // 支付回调
    'getOrderMoney' : '/order/v1/getOrderMoney', // 获取金额
    'getOrderState' : '/order/v1/getOrderState', // 获取状态
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUser (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUser, {
            uid: uid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateUser (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateUser, {
            uid: params.uid,
            name: params.name,
            companyName: params.companyName,
            position: params.position,
            label: params.label,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserOrderList (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserOrderList, {
            uid: uid,
            page: params.page,
            pagesize: params.pagesize,
            key: params.key,
            filteType: params.filteType,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserOrder (oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserOrder, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserEnterprise (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserEnterprise, {
            uid: uid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getEnterpriseName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getEnterpriseName, {
            name: name,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addOrUpdateEnterprise (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addOrUpdateEnterprise, {
            uid: uid,
            type: params.type,
            eid: params.eid,
            eName: params.ename,
            conPhone: params.conPhone,
            conName: params.conName,
            addr: params.addr,
            industry: params.industry,
            enterpriseScale: params.enterpriseScale,
            property: params.property,
            techIntroduction: params.techIntroduction,
            productIntroduction: params.productIntroduction,
            honorQuality: params.honorQuality,
            introduction: params.introduction,
            caseTxt: params.caseTxt,
            buRegistrationNo: params.buRegistrationNo,
            organizationCode: params.organizationCode,
            uniformCreditCode: params.uniformCreditCode,
            businessScope: params.businessScope,
            operationPeriod: params.operationPeriod,
            highTech: params.highTech,
            registeredCapital: params.registeredCapital,
            englishName: params.englishName,
            email: params.email,
            isVillageHighTech: params.isVillageHighTech,
            content: params.content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getIndustryBeanList (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getIndustryBeanList, {
            uid: uid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs (oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: ''
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserNews (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            page: params.page,
            pagesize: params.pagesize,
            uid: params.uid,
            stype: params.stype,
            key: params.key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addUser (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addUser, {
            userName: params.userName,
            userPhone: params.userPhone,
            password: params.password,
            eName: params.eName,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateUserPassword (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateUserPassword, {
            uid: uid,
            password: params.password,
            newPassword: params.newPassword,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getWechatQRCode (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getWechatQRCode, {
            token: '',
            oid: params.oid,
            pid: params.pid,
            attach: params.attach,
            body: params.body,
            detail: params.detail,
            totalFee: params.totalFee,
            width: params.width,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function payBack () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.payBack, {
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getOrderMoney (oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderMoney, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderState (oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderState, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    login,
    getUser,
    updateUser,
    getUserOrderList,
    getUserOrder,
    getUserEnterprise,
    getEnterpriseName,
    addOrUpdateEnterprise,
    getIndustryBeanList,
    getOrderLogs,
    getUserNews,
    addUser,
    updateUserPassword,

    getWechatQRCode,
    payBack,
    getOrderMoney,
    getOrderState
}