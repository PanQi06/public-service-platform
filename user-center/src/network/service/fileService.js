import util from '@/network/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getQINIUToken': 'app/file/v1/getQINIUToken', //获取七牛token
   
};

let res = {

};

function getQINIUToken () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getQINIUToken, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}





export default {
    getQINIUToken,
   
}