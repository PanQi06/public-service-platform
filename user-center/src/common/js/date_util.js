let formatTimestamp = function (timestamp) {
    var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
    var h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours()) + ':';
    var m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes()) + ':';
    var s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds());
    return Y + M + D + h + m + s;
}

let formatTime = function(time) {
	 if (time <= 60) {
        time = time + '秒';
        return time;
    } else if (time > 60 && time < 60 * 60) {
        time = parseInt(time / 60) + "分钟";
        return time;
    } else {
        var hour = parseInt(time / 3600) + "小时";
        var minute = parseInt(parseInt(time % 3600) / 60) + "分钟";
        time = hour + minute;
        return time;
    }
}

export {formatTimestamp, formatTime}
