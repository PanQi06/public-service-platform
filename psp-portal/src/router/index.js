import Vue from 'vue'
import Router from 'vue-router'

import login from '@/view/login/login'
import LoginMainPanel from '@/components/LoginMainPanel'
import home from '@/view/home'
// 首页
import HomePanel from '@/components/HomePanel'
// 待办工作
import workReady from '@/view/workReady/workReady'
// 客户管理
import clientManage from '@/view/client/clientManage'
import clientDetail from '@/view/client/clientDetail'
// 工单管理
import workOrderManage from '@/view/workOrder/workOrderManage'
import workOrderDetail from '@/view/workOrder/workOrderDetail'
// 企业管理
import companyManage from '@/view/company/companyManage'
import companyDetail from '@/view/company/companyDetail'
// 服务分类管理
import serviceClassifyManage from '@/view/serviceClassify/serviceClassifyManage'
// 服务商管理
import serviceProvideManage from '@/view/serviceProvide/serviceProvideManage'
import serviceProvideDetail from '@/view/serviceProvide/serviceProvideDetail'
// 园区管理
import parkManage from '@/view/park/parkManage'
import parkDetail from '@/view/park/parkDetail'
// 客户经理管理
import sellerManage from '@/view/seller/sellerManage'
import sellerDetail from '@/view/seller/sellerDetail'
// 运营账号管理
import adminAccountManage from '@/view/adminAccount/adminAccountManage'
//用户管理
import userInfo from '@/view/userInfo/userInfo'
//文件管理
import fileManage from '@/view/file/fileManage'
//调用api日志管理
import apiManage from '@/view/api/apiManage'


Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'login',
      component: login,
      redirect: '/login',
      children: [{
        path: '/login',
        name: 'loginMainPanel',
        component: LoginMainPanel
      }]
    },
    {
      path: '/home',
      component: home,
      children: [{
          path: '/home',
          name: 'home',
          component: HomePanel
        },
        // 待办工作
        {
          path: '/workReady',
          name: 'workReady',
          component: workReady
        },
        // 客户管理
        {
          path: '/clientManage',
          name: 'clientManage',
          component: clientManage
        },
        {
          path: '/clientDetail',
          name: 'clientDetail',
          component: clientDetail
        },
        // 工单管理
        {
          path: '/workOrderManage',
          name: 'workOrderManage',
          component: workOrderManage
        },
        {
          path: '/workOrderDetail',
          name: 'workOrderDetail',
          component: workOrderDetail
        },
        // 工单管理
        {
          path: '/companyManage',
          name: 'companyManage',
          component: companyManage
        },
        {
          path: '/companyDetail',
          name: 'companyDetail',
          component: companyDetail
        },
        // 园区管理
        {
          path: '/parkManage',
          name: 'parkManage',
          component: parkManage
        },
        {
          path: '/parkDetail',
          name: 'parkDetail',
          component: parkDetail
        },
        // 客户经理管理
        {
          path: '/sellerManage',
          name: 'sellerManage',
          component: sellerManage
        },
        {
          path: '/sellerDetail',
          name: 'sellerDetail',
          component: sellerDetail
        },
        // 服务分类管理
        {
          path: '/serviceClassifyManage',
          name: 'serviceClassifyManage',
          component: serviceClassifyManage
        },
        // 服务商管理
        {
          path: '/serviceProvideManage',
          name: 'serviceProvideManage',
          component: serviceProvideManage
        },
        {
          path: '/serviceProvideDetail',
          name: 'serviceProvideDetail',
          component: serviceProvideDetail
        },
        // 运营账号管理
        {
          path: '/adminAccountManage',
          name: 'adminAccountManage',
          component: adminAccountManage
        },
        // 账号管理
        {
          path: '/userInfo',
          name: 'userInfo',
          component: userInfo
        },
        // 文件管理
        {
          path: '/fileManage',
          name: 'fileManage',
          component: fileManage
        },
        // 调用api日志管理
        {
          path: '/apiManage',
          name: 'apiManage',
          component: apiManage
        }
      ]
    }
  ]
})
