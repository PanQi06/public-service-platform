import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);


const state = {
    pageStack: [],
    userInfo: {}
};

export default new Vuex.Store({
    state
});
