import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/admin/v1/login',
    'updatePassWord': '/admin/v1/updatePassWord',//修改密码
    'updateName': '/admin/v1/updateName',//修改名称
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
            imgCode: params.imgCode
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updatePassWord (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updatePassWord, {
            oldPwd: params.oldPwd,
            password: params.password,
            confirmPwd: params.confirmPwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    login,updateName,updatePassWord
}