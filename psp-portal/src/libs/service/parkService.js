import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getList': '/park/v1/getList', //获取园区列表
    'eidtPark': '/park/v1/eidt', //编辑园区
    'delPark': '/park/v1/del', //删除园区

    'getAllArea': '/area/v1/getAllArea', //获取全国地区接口
};

let res = {

};
function getList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {
            page: params.currentPage,
            pagesize: params.pagesize,
            key: params.key,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function eidtPark(pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtPark, {
            pid: pid,
            name: params.name,
            contact: params.contact,
            phoneNum: params.phoneNum,
            cityCode: params.cityCode,
            province: params.province,
            city: params.city,
            district: params.district,
            areaArray: params.areaArray,
            coordinate: params.coordinate,
            brief: params.brief
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delPark(pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delPark, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getAllArea(pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAllArea, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getList, eidtPark, delPark, getAllArea
}