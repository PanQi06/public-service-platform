import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getUsers': '/user/v1/getUsers', // 客户列表
    'getUserNum': '/user/v1/getUserNum', //获取客户总数
    'allot': '/user/v1/allot',//分配客户给销售
    'getDetail': '/user/v1/getDetail', //获取客户详情
    'getUserLogs': '/user/v1/getUserLogs',//获取客户操作日志
    'getUserNews': '/user/v1/getUserNews',//获取客户消息流
    'getCompanies': '/user/v1/getCompanies',//获取企业列表
    'getCompany': '/user/v1/getCompany',//获取企业详情
    'exportCompanies': '/user/v1/exportCompanies',//导出

};

let res = {

};

function getUsers (params, sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType,
            isAllot: params.isAllot,
            sid: sid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function exportCompanies (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.exportCompanies, {
            page: params.currentPage,
            pagesize: params.pagesize,
            industry: params.industry,
            key: params.key,
            area: params.area,
            isHighTech: params.isHighTech,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserNum (isAllot) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNum, {
            isAllot: isAllot
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function allot (uid, sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.allot, {
            uid: uid,
            sid: sid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getDetail (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            uid: uid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserLogs (uid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserLogs, {
            uid: uid,
            key: key
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserNews (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            uid: uid,
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getCompanies (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getCompanies, {
            page: params.currentPage,
            pagesize: params.pagesize,
            key: params.key,
            industry: params.industry,
            area: params.area,
            isHighTech: params.isHighTech,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getCompany (eid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getCompany, {
            eid: eid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getUsers: getUsers,
    getUserNum: getUserNum,
    allot: allot,
    getDetail: getDetail,
    getUserLogs: getUserLogs,
    getUserNews: getUserNews,
    getCompanies: getCompanies,
    getCompany: getCompany,
    exportCompanies: exportCompanies,
}