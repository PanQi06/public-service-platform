import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getQINIUToken': 'file/v1/getQINIUToken', //获取七牛token
    'getlist': 'file/v1/getlist', // 获取文件列表
    'exportFile': 'file/v1/exportFile', // 下载导出文件列表


    'getlogs': 'share/v1/getlogs', // 下载导出文件列表
   
};

let res = {

};

function getQINIUToken () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getQINIUToken, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getlist (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getlist, {
            pageSize: params.pagesize,
            marker: params.marker,
            prefix: params.key,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function exportFile (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.exportFile, {
            pageSize: params.pageSize,
            marker: params.marker,
            prefix: params.key,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getlogs (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getlogs, {
            page: params.currentPage,
            pageSize: params.pageSize,
            name: params.name,
            shareName: params.shareName,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}





export default {
    getQINIUToken,
    getlist,
    exportFile,
    getlogs
}