import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'addProvide': '/provider/v1/add', //创建服务商并生成管理员账户
    'eidtProvider': '/provider/v1/eidtProvider',//编辑服务商
    'addService': '/provider/v1/addService',//添加服务
    'delService': '/provider/v1/delService',//编辑服务
    'getServiceList': '/provider/v1/getServiceList',//获取服务商服务列表
    'getList': '/provider/v1/getList', //获取服务商列表
    'delProvide': '/provider/v1/del',//删除服务商
    'getDetail': '/provider/v1/getDetail',//获取服务商信息
    'getAccountList': '/provider/v1/getAccountList',//获取服务商账号列表
    'addAccount': '/provider/v1/addAccount',//创建服务商子账号
    'restPwd': '/provider/v1/restPwd',//重置密码
    'delAccount': '/provider/v1/delAccount', //删除子账户
};

let res = {

};
function addProvide (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addProvide, {
            name: params.name,
            address: params.address,
            contact: params.contact,
            phoneNum: params.phoneNum,
            password: params.pwd,
            confirmPwd: params.submitPwd,
            content: params.content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function eidtProvider (pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtProvider, {
            pid: pid,
            name: params.name,
            address: params.address,
            contact: params.contact,
            phoneNum: params.phoneNum,
            content: params.content,
            cid: params.cid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addService (pid, cid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addService, {
            pid: pid,
            cid: cid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delService (pid, cid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delService, {
            pid: pid,
            cid: cid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getList (page, pagesize, cid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {
            page: page,
            pagesize: pagesize,
            cid: cid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delProvide (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delProvide, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getServiceList (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getServiceList, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getDetail (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getAccountList (page, pagesize, pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccountList, {
            page: page,
            pagesize: pagesize,
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addAccount (params, pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addAccount, {
            name: params.name,
            phone: params.phone,
            password: params.password,
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function restPwd (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.restPwd, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delAccount (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delAccount, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    addProvide,eidtProvider,addService, delService,getServiceList,getList,delProvide,getDetail,getAccountList,addAccount,restPwd,delAccount
}