import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/admin/v1/login', // 登录
    'getAdmin': '/admin/v1/getAdmin', //获取管理员信息
    'updateName': '/admin/v1/updateName',//更新用户名
    'updatePassWord': '/admin/v1/updatePassWord', //更新密码
    'getList': '/admin/v1/getList',//获取管理员列表
    'eidtAdmin': '/admin/v1/eidt',//新增、编辑管理员
    'restAdminPwd': '/admin/v1/restAdminPwd',//重置密码
    'delAdmin': '/admin/v1/del',//删除管理员

    'getOrderStatistics': '/admin/v1/getOrderStatistics',//获取工单统计数
    'getUserStatistics': '/admin/v1/getUserStatistics',//获取用户统计数
};

let res = {

};

function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
            imgCode: params.imgCode,
            device: params.device
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getAdmin () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAdmin, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function updatePassWord (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updatePassWord, {
            oldPwd: params.oldPwd,
            newPwd: params.newPwd,
            submitPwd: params.submitPwd
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {
            key: params.key,
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function eidtAdmin (aid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtAdmin, {
            aid: aid,
            username: params.username,
            phoneNum: params.phoneNum,
            password: params.pwd,
            confirmPwd: params.submitPwd,
            type: params.type,
            pid: params.pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function restAdminPwd (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.restAdminPwd, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderStatistics () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderStatistics, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserStatistics () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserStatistics, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

export default {
    login,getAdmin,updateName,updatePassWord,getList,eidtAdmin,restAdminPwd,getOrderStatistics,getUserStatistics
}