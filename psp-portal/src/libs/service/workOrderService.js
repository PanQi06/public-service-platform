import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    //工单相关接口
    'getOrders': '/order/v1/getOrders', //获取工单信息列表
    'getOrderNum': '/order/v1/getOrderNum', //获取工单数量
    'getDetail': '/order/v1/getDetail', //获取工单详情
    'getOrderLogs': '/order/v1/getOrderLogs', //获取工单操作日志
    'exportOrders': '/order/v1/exportOrders', //导出
};

let res = {

};
function getOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stage: params.stage,
            ttype: params.ttype,
            targetId: params.targetId,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType,
            dataType: params.dataType,
            startTime: params.startTime,
            endTime: params.endTime,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function exportOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.exportOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stage: params.stage,
            ttype: params.ttype,
            targetId: params.targetId,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType,
            dataType: params.dataType,
            startTime: params.startTime,
            endTime: params.endTime,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderNum(stage) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderNum, {
            stage: stage
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs(oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

export default {
    getOrders: getOrders,
    getOrderNum: getOrderNum,
    getDetail: getDetail,
    getOrderLogs: getOrderLogs,
    exportOrders: exportOrders
}