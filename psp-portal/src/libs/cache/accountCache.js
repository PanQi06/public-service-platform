function getAdminToken() {
    return localStorage.getItem('adminToken')
}
function setAdminToken(token) {
    localStorage.setItem('adminToken', token)
}
function clearAdminToken() {
    return localStorage.removeItem('adminToken')
}

function getAdminInfo() {
    return localStorage.getItem('PortalUserInfo')
}
function setAdminInfo(adminInfo) {
    localStorage.setItem('PortalUserInfo', adminInfo)
}
function clearAdminInfo() {
    return localStorage.removeItem('PortalUserInfo')
}

function getAdminUserName() {
    return localStorage.getItem('adminUserName')
}
function setAdminUserName(userName) {
    localStorage.setItem('adminUserName', userName)
}
function clearAdminUserName() {
    return localStorage.removeItem('adminUserName')
}

function getAdminPwd() {
    return localStorage.getItem('adminPwd')
}
function setAdminPwd(pwd) {
    localStorage.setItem('adminPwd', pwd)
}
function clearAdminPwd() {
    return localStorage.removeItem('adminPwd')
}

export default {
   setAdminToken,
   getAdminToken,
   clearAdminToken,
   setAdminInfo,
   getAdminInfo,
   clearAdminInfo,
   setAdminUserName,
   getAdminUserName,
   clearAdminUserName,
   setAdminPwd,
   getAdminPwd,
   clearAdminPwd
}