let formatActionType = function(type) {
	if (type == 0) {
        return '创建并分配';
    } else if (type == 1) {
        return '编辑';
    } else if (type == 2) {
        return '派单';
    } else if (type == 3) {
        return '上传合同';
    } else if (type == 4) {
        return '确认完成';
    } else if (type == 5) {
        return '拒绝完成';
    } else if (type == 6) {
        return '调查反馈'
    } else if (type == 7) {
        return '归档关闭'
    } else if (type == 8) {
        return '接受'
    } else if (type == 9) {
        return '拒绝'
    } else if (type == 10) {
        return '申请完成'
    } else if (type == 11) {
        return '申请终止'
    } else if (type == 12) {
        return '支付完成'
    }
}

let formatAction = function(data) {
    let type = data.type;
    if (type == 0) {
        let sellerJson = JSON.parse(data.sellerJson);
        if (data.providerJson != '' && data.providerJson != null) {
            let providerJson = JSON.parse(data.providerJson);
            return '客户经理【' + sellerJson.name + '】创建工单并分配给服务商【'+ providerJson.name +'】';
        } else {
            return '客户经理【' + sellerJson.name + '】创建工单';
        }
        
    } else if (type == 1) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '客户经理【'+ sellerJson.name +'】编辑工单'
    } else if (type == 2) {
        let sellerJson = JSON.parse(data.sellerJson);
        let providerJson = JSON.parse(data.providerJson);
        return '客户经理【'+ sellerJson.name +'】将工单派送给：' + providerJson.name;
    } else if (type == 3) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '客户经理【'+ sellerJson.name +'】上传了合同';
    } else if (type == 4) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '客户经理【' + sellerJson.name + '】确认完成工单';
    } else if (type == 5) {
        let sellerJson = JSON.parse(data.sellerJson);
        let content = JSON.parse(data.content);
        return '客户经理【' + sellerJson.name + '】拒绝完成工单，理由是' + content.content;
    } else if (type == 6) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '客户经理【' + sellerJson.name + '】填写调查反馈';
    } else if (type == 7) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '客户经理【' + sellerJson.name + '】将工单归档关闭';
    } else if (type == 8) {
        let providerJson = JSON.parse(data.providerJson);
        return '服务商【' + providerJson.name + '】，' + providerJson.contact + '接受工单';
    } else if (type == 9) {
        let providerJson = JSON.parse(data.providerJson);
        let content = JSON.parse(data.content);
        return '服务商【' + providerJson.name + '】拒绝接受此工单，理由是：' + content.content;
    } else if (type == 10) {
        let providerJson = JSON.parse(data.providerJson);
        return '服务商【' + providerJson.name + '】申请完成工单';
    } else if (type == 11) {
        let sellerJson = JSON.parse(data.sellerJson);
        return '服务商【' + sellerJson.name + '】申请终止工单';
    } else if (type == 12) {
        let content = JSON.parse(data.content);
        return '客户【' + content.name + '】支付完成';
    }
}

export {formatActionType, formatAction}