let formatActionType = function(type) {
	if (type == 0) {
        return '分配客户';
    } else if (type == 1) {
        return '新建客户';
    } else if (type == 2) {
        return '修改客户';
    } else if (type == 3) {
        return '归档客户';
    } else if (type == 4) {
        return '修改评级';
    } else if (type == 5) {
        return '修改标签';
    } else if (type == 6) {
        return '开始沟通';
    }
}

let formatAction = function(data) {
    let type = data.type;
    let seller = JSON.parse(data.sellerJson);
	if (type == 0) {
        let admin = JSON.parse(data.adminJson);
        return '【' + admin.name + '】分配客户给客户经理【'+ seller.name +'】';
    } else if (type == 1) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】创建了客户，客户姓名：' + user.name + '，电话：' + user.phoneNum + '，公司名称：' + user.companyName + '，职位：' + user.position;
    } else if (type == 2) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】修改了客户，客户姓名：' + user.name + '，电话：' + user.phoneNum + '，公司名称：' + user.companyName + '，职位：' + user.position;
    } else if (type == 3) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】归档了客户';
    } else if (type == 4) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】修改了客户级别，客户级别为：';
    } else if (type == 5) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】修改了客户标签，客户标签为：';
    } else if (type == 6) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】与客户【' + user.name + '】开始沟通';
    }
}

export {formatActionType, formatAction}