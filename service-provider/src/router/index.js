import Vue from 'vue'
import Router from 'vue-router'

import login from '@/view/login/login'
import LoginMainPanel from '@/components/LoginMainPanel'

import main from '@/view/Main'
import workReady from '@/view/workReady/workReady'
import subuserInfoManage from '@/view/subUserInfo/subuserInfoManage'
import userInfo from '@/view/account/userInfo'

import workOrderManage from '@/view/workOrder/workOrderManage'
import workOrderDetail from	'@/view/workOrder/workOrderDetail'



Vue.use(Router)

export default new Router({
	routes: [
	{
		path: '/',
		name: 'login',
		component: login,
		redirect: '/login',
		children: [
			{
				path: '/login',
				name: 'loginMainPanel',
				component: LoginMainPanel
			}
		]
	},
	{
		path: '/main',
		name: 'main',
		component: main,
		redirect: '/main/workReady',
		children: [
		{
			path: '/main/workReady',
			name: 'workReady',
			component: workReady
		},
		{
			path: '/main/workOrderManage',
			name: 'workOrderManage',
			component: workOrderManage,
		},
		{
			path: '/main/workOrderDetail/',
			name: 'workOrderDetail',
			component: workOrderDetail
		},
		{
			path: '/main/userInfo',
			name: 'userInfo',
			component: userInfo
		},
		{
			path: '/main/subuserInfoManage',
			name: 'subuserInfoManage',
			component: subuserInfoManage
		}
		]
	}
	]
})
