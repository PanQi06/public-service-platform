function getToken() {
    return localStorage.getItem('providerToken')
}
function setToken(token) {
    localStorage.setItem('providerToken', token)
}
function clearToken() {
    return localStorage.removeItem('providerToken')
}

function getInfo() {
    return localStorage.getItem('ServiceUserInfo')
}
function setInfo(adminInfo) {
    localStorage.setItem('ServiceUserInfo', adminInfo)
}
function clearInfo() {
    return localStorage.removeItem('ServiceUserInfo')
}

function getUserName() {
    return localStorage.getItem('providerUserName')
}
function setUserName(userName) {
    localStorage.setItem('providerUserName', userName)
}
function clearUserName() {
    return localStorage.removeItem('providerUserName')
}

function getPwd() {
    return localStorage.getItem('providerPwd')
}
function setPwd(pwd) {
    localStorage.setItem('providerPwd', pwd)
}
function clearPwd() {
    return localStorage.removeItem('providerPwd')
}

export default {
   setToken,
   getToken,
   clearToken,
   setInfo,
   getInfo,
   clearInfo,
   setUserName,
   getUserName,
   clearUserName,
   setPwd,
   getPwd,
   clearPwd
}