import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/account/v1/login',
    'resetPwd': '/account/v1/resetPwd',//更改密码
    'updateName': '/account/v1/updateName',//更改用户名
    'getAccountList': '/account/v1/getAccountList',//获取服务商列表
    'addAccount': '/account/v1/addAccount',//创建服务商账号
    'resetAccountPwd': '/account/v1/resetAccountPwd',//重置服务商账号密码
    'delAccount': '/account/v1/delAccount',//删除服务商账号
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
            imgCode: params.imgCode
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function resetPwd (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetPwd, {
            oldPwd: params.oldPwd,
            password: params.password,
            confirmPwd: params.confirmPwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getAccountList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccountList, {
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addAccount (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addAccount, {
            name: params.name,
            phone: params.phone,
            password: params.pwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function resetAccountPwd (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetAccountPwd, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delAccount (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delAccount, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    login,resetPwd,updateName,getAccountList,addAccount,resetAccountPwd,delAccount
}