import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getUsers': '/user/v1/getUsers', // 客户列表
    'getUserNum': '/user/v1/getUserNum', //获取客户总数
    'add': '/user/v1/add', //新增客户
    'edit': '/user/v1/edit', //编辑客户
    'editLabel': '/user/v1/editLabel', //编辑标签
    'editLevel': '/user/v1/editLevel', //编辑客户级别
    'archive': '/user/v1/archive', //归档客户
    'getDetail': '/user/v1/getDetail', //获取客户详情
    'getUserLogs': '/user/v1/getUserLogs',//获取客户操作日志
    //客户消息相关接口
    'getUserNews': '/usernews/v1/getUserNews',//获取客户消息流
    'addClientNews': '/usernews/v1/add',//新增客户消息

};

let res = {

};

function getUsers (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType,
            status: params.status
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function addClient (params,label, isUpdate, isClaim) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.add, {
            name: params.name,
            phoneNum: params.phoneNum,
            companyName: params.companyName,
            position: params.position,
            label: label,
            isUpdate: isUpdate,
            isClaim: isClaim
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function editClient (params, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.edit, {
            userId: params.uid,
            name: params.name,
            phoneNum: params.phoneNum,
            companyName: params.companyName,
            position: params.position,
            label: label
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function editLabel (userId, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLabel, {
            userId: userId,
            label: label
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function editLevel (userId, level) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLevel, {
            userId: userId,
            level: level
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function archive (userId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.archive, {
            userId: userId
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            uid: uid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserLogs (uid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserLogs, {
            uid: uid,
            key: key
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserNews (uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            uid: uid,
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addClientNews(uid, label, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addClientNews, {
            uid: uid,
            label: label,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getUsers: getUsers,
    addClient: addClient,
    editClient: editClient,
    editLabel: editLabel,
    editLevel: editLevel,
    archive: archive,
    getDetail: getDetail,
    getUserLogs: getUserLogs,
    getUserNews: getUserNews,
    addClientNews: addClientNews
}